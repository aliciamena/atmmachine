package storage;
import java.sql.*;

public class DBController {
    private static final String URL = "jdbc:mysql://localhost/mydb";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "Adapajarito0206";

    public static Connection getConn() throws SQLException {
        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public static ResultSet select(String SQLQuery) {
        ResultSet resultats = null;
        try {
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
            resultats = stmt.executeQuery(SQLQuery);
        } catch(SQLException e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        
        return resultats;

    }



}