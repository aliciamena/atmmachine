package models;

import java.util.Date;

public class Cuenta {
    private Long idCuentaCorriente;
    private Double SaldoDisponible;
    private String Nombre;
    private String IBAN;
    private String NumeroCuenta;
    private int Interes=0;
    private Date FechaCreacion;
    
    
    public Cuenta(Long idCuentaCorriente, Double saldoDisponible, String nombre, String iBAN, String numeroCuenta,
            int interes, Date fechaCreacion) {
        this.idCuentaCorriente = idCuentaCorriente;
        this.SaldoDisponible = saldoDisponible;
        this.Nombre = nombre;
        this.IBAN = iBAN;
        this.NumeroCuenta = numeroCuenta;
        this.Interes = interes;
        this.FechaCreacion = fechaCreacion;
    }


    public Long getIdCuentaCorriente() {
        return idCuentaCorriente;
    }
    public void setIdCuentaCorriente(Long idCuentaCorriente) {
        this.idCuentaCorriente = idCuentaCorriente;
    }
    public Double getSaldoDisponible() {
        return SaldoDisponible;
    }
    public void setSaldoDisponible(Double saldoDisponible) {
        SaldoDisponible = saldoDisponible;
    }
    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String nombre) {
        Nombre = nombre;
    }
    public String getIBAN() {
        return IBAN;
    }
    public void setIBAN(String iBAN) {
        IBAN = iBAN;
    }
    public String getNumeroCuenta() {
        return NumeroCuenta;
    }
    public void setNumeroCuenta(String numeroCuenta) {
        NumeroCuenta = numeroCuenta;
    }
    public int getInteres() {
        return Interes;
    }
    public void setInteres(int interes) {
        Interes = interes;
    }
    public Date getFechaCreacion() {
        return FechaCreacion;
    }
    public void setFechaCreacion(Date fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }
    @Override
    public String toString() {
        return "Cuenta [FechaCreacion=" + FechaCreacion + ", IBAN=" + IBAN + ", Interes=" + Interes + ", Nombre="
                + Nombre + ", NumeroCuenta=" + NumeroCuenta + ", SaldoDisponible=" + SaldoDisponible
                + ", idCuentaCorriente=" + idCuentaCorriente + "]";
    }

    
}
