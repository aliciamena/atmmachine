package models;

import java.util.Date;

public class Financiacion {
    private Long idFinanciacion;
    private Float valor;
    private Date fechaConcesion;
    private int duracion=0;
    private String nombre;
    private Long idCuentaCorriente;
    private Long idCredito;
    private Date fechaLimite;
    
    public Financiacion(Long idFinanciacion, Float valor, Date fechaConcesion, int duracion, String nombre,
            Long idCuentaCorriente, Long idCredito, Date fechaLimite) {
        this.idFinanciacion = idFinanciacion;
        this.valor = valor;
        this.fechaConcesion = fechaConcesion;
        this.duracion = duracion;
        this.nombre = nombre;
        this.idCuentaCorriente = idCuentaCorriente;
        this.idCredito = idCredito;
        this.fechaLimite = fechaLimite;
    }

    public Long getIdFinanciacion() {
        return idFinanciacion;
    }
    public void setIdFinanciacion(Long idFinanciacion) {
        this.idFinanciacion = idFinanciacion;
    }
    public Float getValor() {
        return valor;
    }
    public void setValor(Float valor) {
        this.valor = valor;
    }
    public Date getFechaConcesion() {
        return fechaConcesion;
    }
    public void setFechaConcesion(Date fechaConcesion) {
        this.fechaConcesion = fechaConcesion;
    }
    public int getDuracion() {
        return duracion;
    }
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Long getIdCuentaCorriente() {
        return idCuentaCorriente;
    }
    public void setIdCuentaCorriente(Long idCuentaCorriente) {
        this.idCuentaCorriente = idCuentaCorriente;
    }
    public Long getIdCredito() {
        return idCredito;
    }
    public void setIdCredito(Long idCredito) {
        this.idCredito = idCredito;
    }
    public Date getFechaLimite() {
        return fechaLimite;
    }
    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    @Override
    public String toString() {
        return "Financiacion [duracion=" + duracion + ", fechaConcesion=" + fechaConcesion + ", fechaLimite="
                + fechaLimite + ", idCredito=" + idCredito + ", idCuentaCorriente=" + idCuentaCorriente
                + ", idFinanciacion=" + idFinanciacion + ", nombre=" + nombre + ", valor=" + valor + "]";
    }


}
