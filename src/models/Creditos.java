package models;

public class Creditos {
    private Long idCredito;
    private String tipo;
    private int limite=0;

    public Creditos(Long idCredito, String tipo, int limite) {
        this.idCredito = idCredito;
        this.tipo = tipo;
        this.limite = limite;
    }

    public Long getIdCredito() {
        return idCredito;
    }
    public void setIdCredito(Long idCredito) {
        this.idCredito = idCredito;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public int getLimite() {
        return limite;
    }
    public void setLimite(int limite) {
        this.limite = limite;
    }

    @Override
    public String toString() {
        return "Creditos [idCredito=" + idCredito + ", limite=" + limite + ", tipo=" + tipo + "]";
    }
  
}
