package models;

public class Titular {
    
    private Long idTitular;
    private String apellido1;
    private String apellido2=null;
    private String nombre;
    private String dni;
    private String teléfono;
    private String correo;
    private String situacionLaboral;
    private String estadoCivil;
    private String nacionalidad;
    private Long idDireccion;
    private int pin;
    
    public Titular(Long idTitular, String apellido1, String apellido2, String nombre, String dni, String teléfono,
            String correo, String situacionLaboral, String estadoCivil, String nacionalidad, Long idDireccion,
            int pin) {
        this.idTitular = idTitular;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.nombre = nombre;
        this.dni = dni;
        this.teléfono = teléfono;
        this.correo = correo;
        this.situacionLaboral = situacionLaboral;
        this.estadoCivil = estadoCivil;
        this.nacionalidad = nacionalidad;
        this.idDireccion = idDireccion;
        this.pin = pin;
    }

    public Long getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(Long idTitular) {
        this.idTitular = idTitular;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTeléfono() {
        return teléfono;
    }

    public void setTeléfono(String teléfono) {
        this.teléfono = teléfono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSituacionLaboral() {
        return situacionLaboral;
    }

    public void setSituacionLaboral(String situacionLaboral) {
        this.situacionLaboral = situacionLaboral;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Long getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "Titular [apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", correo=" + correo + ", dni=" + dni
                + ", estadoCivil=" + estadoCivil + ", idDireccion=" + idDireccion + ", idTitular=" + idTitular
                + ", nacionalidad=" + nacionalidad + ", nombre=" + nombre + ", pin=" + pin + ", situacionLaboral="
                + situacionLaboral + ", teléfono=" + teléfono + "]";
    }

}
