package models;

public class Direcciones {
    private Long idDireccion;
    private String poblacion;
    private String cPostal;
    private String direccion;
    private String numero;
    private String piso=null;
    private String provincia;
    private String pais;
    
    public Direcciones(Long idDireccion, String poblacion, String cPostal, String direccion, String numero, String piso,
            String provincia, String pais) {
        this.idDireccion = idDireccion;
        this.poblacion = poblacion;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.numero = numero;
        this.piso = piso;
        this.provincia = provincia;
        this.pais = pais;
    }

    public Long getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getcPostal() {
        return cPostal;
    }

    public void setcPostal(String cPostal) {
        this.cPostal = cPostal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return "Direcciones [cPostal=" + cPostal + ", direccion=" + direccion + ", idDireccion=" + idDireccion
                + ", numero=" + numero + ", pais=" + pais + ", piso=" + piso + ", poblacion=" + poblacion
                + ", provincia=" + provincia + "]";
    }
    
}
