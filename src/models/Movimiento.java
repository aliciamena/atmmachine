package models;

import java.util.Date;

public class Movimiento {
    private Long idMovimiento;
    private double Valor;
    private Date Fecha;
    private String Emisor;
    private String Concepto;
    private String Periodicidad=null;
    private Long idCuentaCorriente;
    private Long idCategoria;
    private Long idTarjeta=null;
    
    public Movimiento(Long idMovimiento, double valor, Date fecha, String emisor, String concepto, String periodicidad,
            Long idCuentaCorriente, Long idCategoria, Long idTarjeta) {
        this.idMovimiento = idMovimiento;
        Valor = valor;
        Fecha = fecha;
        Emisor = emisor;
        Concepto = concepto;
        Periodicidad = periodicidad;
        this.idCuentaCorriente = idCuentaCorriente;
        this.idCategoria = idCategoria;
        this.idTarjeta = idTarjeta;
    }

    public Long getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(Long idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public double getValor() {
        return Valor;
    }

    public void setValor(double valor) {
        Valor = valor;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public String getEmisor() {
        return Emisor;
    }

    public void setEmisor(String emisor) {
        Emisor = emisor;
    }

    public String getConcepto() {
        return Concepto;
    }

    public void setConcepto(String concepto) {
        Concepto = concepto;
    }

    public String getPeriodicidad() {
        return Periodicidad;
    }

    public void setPeriodicidad(String periodicidad) {
        Periodicidad = periodicidad;
    }

    public Long getIdCuentaCorriente() {
        return idCuentaCorriente;
    }

    public void setIdCuentaCorriente(Long idCuentaCorriente) {
        this.idCuentaCorriente = idCuentaCorriente;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Long getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(Long idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    @Override
    public String toString() {
        return "Movimiento [Concepto=" + Concepto + ", Emisor=" + Emisor + ", Fecha=" + Fecha + ", Periodicidad="
                + Periodicidad + ", Valor=" + Valor + ", idCategoria=" + idCategoria + ", idCuentaCorriente="
                + idCuentaCorriente + ", idMovimiento=" + idMovimiento + ", idTarjeta=" + idTarjeta + "]";
    }
    

    

    
}
