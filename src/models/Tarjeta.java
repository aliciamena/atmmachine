package models;

public class Tarjeta {
    private Long idTarjeta;
    private String Nombre;
    private String NumTarjeta;
    private String FechaCaducidad;
    private String NumSeguridad;
    private String Tipo;
    private int idCuentaCorriente;
    private Long idFinanciacion=null;
    private Long idTitular;
    
    public Tarjeta(Long idTarjeta, String nombre, String numTarjeta, String fechaCaducidad, String numSeguridad,
            String tipo, int idCuentaCorriente, Long idFinanciacion, Long idTitular) {
        this.idTarjeta = idTarjeta;
        Nombre = nombre;
        NumTarjeta = numTarjeta;
        FechaCaducidad = fechaCaducidad;
        NumSeguridad = numSeguridad;
        Tipo = tipo;
        this.idCuentaCorriente = idCuentaCorriente;
        this.idFinanciacion = idFinanciacion;
        this.idTitular = idTitular;
    }

    public Long getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(Long idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNumTarjeta() {
        return NumTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        NumTarjeta = numTarjeta;
    }

    public String getFechaCaducidad() {
        return FechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        FechaCaducidad = fechaCaducidad;
    }

    public String getNumSeguridad() {
        return NumSeguridad;
    }

    public void setNumSeguridad(String numSeguridad) {
        NumSeguridad = numSeguridad;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public int getIdCuentaCorriente() {
        return idCuentaCorriente;
    }

    public void setIdCuentaCorriente(int idCuentaCorriente) {
        this.idCuentaCorriente = idCuentaCorriente;
    }

    public Long getIdFinanciacion() {
        return idFinanciacion;
    }

    public void setIdFinanciacion(Long idFinanciacion) {
        this.idFinanciacion = idFinanciacion;
    }

    public Long getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(Long idTitular) {
        this.idTitular = idTitular;
    }

    @Override
    public String toString() {
        return "Tarjeta [FechaCaducidad=" + FechaCaducidad + ", Nombre=" + Nombre + ", NumSeguridad=" + NumSeguridad
                + ", NumTarjeta=" + NumTarjeta + ", Tipo=" + Tipo + ", idCuentaCorriente=" + idCuentaCorriente
                + ", idFinanciacion=" + idFinanciacion + ", idTarjeta=" + idTarjeta + ", idTitular=" + idTitular + "]";
    }
    
     
}
