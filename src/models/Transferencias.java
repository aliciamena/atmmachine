package models;

public class Transferencias {
    private Long idCuentaCorriente;
    private Long idMovimiento;
    private String ibanDestino;
    private String numeroCuentaDestino;
    
    public Transferencias(Long idCuentaCorriente, Long idMovimiento, String ibanDestino, String numeroCuentaDestino) {
        this.idCuentaCorriente = idCuentaCorriente;
        this.idMovimiento = idMovimiento;
        this.ibanDestino = ibanDestino;
        this.numeroCuentaDestino = numeroCuentaDestino;
    }
    
    public Long getIdCuentaCorriente() {
        return idCuentaCorriente;
    }
    public void setIdCuentaCorriente(Long idCuentaCorriente) {
        this.idCuentaCorriente = idCuentaCorriente;
    }
    public Long getIdMovimiento() {
        return idMovimiento;
    }
    public void setIdMovimiento(Long idMovimiento) {
        this.idMovimiento = idMovimiento;
    }
    public String getIbanDestino() {
        return ibanDestino;
    }
    public void setIbanDestino(String ibanDestino) {
        this.ibanDestino = ibanDestino;
    }
    public String getNumeroCuentaDestino() {
        return numeroCuentaDestino;
    }
    public void setNumeroCuentaDestino(String numeroCuentaDestino) {
        this.numeroCuentaDestino = numeroCuentaDestino;
    }

    @Override
    public String toString() {
        return "Transferencias [ibanDestino=" + ibanDestino + ", idCuentaCorriente=" + idCuentaCorriente
                + ", idMovimiento=" + idMovimiento + ", numeroCuentaDestino=" + numeroCuentaDestino + "]";
    }


}
