package models;

public class Intereses {
    private Long idInteres;
    private String tipo;
    private int interes;
    
    public Intereses(Long idInteres, String tipo, int interes) {
        this.idInteres = idInteres;
        this.tipo = tipo;
        this.interes = interes;
    }

    public Long getIdInteres() {
        return idInteres;
    }
    public void setIdInteres(Long idInteres) {
        this.idInteres = idInteres;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public int getInteres() {
        return interes;
    }
    public void setInteres(int interes) {
        this.interes = interes;
    }

    @Override
    public String toString() {
        return "Intereses [idInteres=" + idInteres + ", interes=" + interes + ", tipo=" + tipo + "]";
    }
  
}
