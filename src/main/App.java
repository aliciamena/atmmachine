package main;
import java.io.*;

import controllers.*;

public class App {
    public static boolean sesion = false, acabar = false;

    public static void main(String[] args) {
        // Create the console object
        Console cnsl = System.console();
    
        if (cnsl == null) {
            System.out.println(
                "No console available");
            return;
        }

        
        while (sesion == false) {
            
            //Login user
            Session list = new Session();
            boolean aux = list.login();
            System.out.println(aux);
            System.out.println(acabar);
            

            if(aux==true){                   
                Movimientos.verSaldo();

                while (acabar == false) {
                    
                    // Menu options
                    System.out.println(
                        "Elija la operacion que desea realizar.");
                    System.out.println(
                        "Número 1. Cambiar pin.");
                    System.out.println(
                        "Número 2. Lista de movimientos");
                    System.out.println(
                        "Número 3. Ingresar efectivo.");
                    System.out.println(
                        "Número 4. Retirar efectivo.");
                    System.out.println(
                        "Número 5. Realizar transferencia.");
                    System.out.println(
                        "Número 6. Cerrar sesión.");
            
                    String option = cnsl.readLine();
                    switch (option) {
                        case "1": //Cambiar pin
        
                            Session change = new Session();
                            change.changePin();

                            break;
            
                        case "2": //Lista de movimientos
            
                            System.out.println("Lista de movimientos: ");
            
                            Movimientos lista = new Movimientos();

                            String ibanL = cnsl.readLine(
                                "Ingresa el IBAN de la cuenta que quieras consultar los movimientos: ");
                            String numCuentaL = cnsl.readLine(
                                "Ingresa el número de la cuenta que quieras consultar los movimientos: ");                               
                                
                            int idCuCoL = lista.obtenerIdCuenta(ibanL, numCuentaL);

                            lista.listaMovimientos(idCuCoL);
                            
                            break;
        
                        case "3": //Ingresar efectivo

                            Movimientos mov = new Movimientos();

                            String iban = cnsl.readLine(
                                "Ingresa el IBAN de la cuenta a la que quieras ingresar: ");
                            String numCuenta = cnsl.readLine(
                                "Ingresa el número de la cuenta a la que quieras ingresar: ");                               
                                
                            int idCuCo = mov.obtenerIdCuenta(iban, numCuenta);
                            mov.ingresar(idCuCo);
                            Movimientos.update(idCuCo);
                            break;
            
                        case "4": //Retirar efectivo

                            Movimientos movret = new Movimientos();
                            
                            String ibanR = cnsl.readLine(
                                "Ingresa el IBAN de la cuenta que quieras retirar: ");
                            String numCuentaR = cnsl.readLine(
                                "Ingresa el número de la cuenta de la que quieras retirar: ");                               
                                
                            int idCuCoR = movret.obtenerIdCuenta(ibanR, numCuentaR);

                            movret.retirar(idCuCoR);
                            break;
        
                        case "5": //Transferir dinero
        
                            Movimientos movtra = new Movimientos();

                            String ibanTra = cnsl.readLine(
                                "Ingresa el IBAN de la cuenta de la que quieras transferir: ");
                            String numCuentaTra = cnsl.readLine(
                                "Ingresa el número de la cuenta de la quieras transferir: ");                               
                                
                            int idCuCoTra = movtra.obtenerIdCuenta(ibanTra, numCuentaTra);
                            movtra.transferir(idCuCoTra);

                            break;
    
                        case "6": //Cerrar sesión
        
                            System.out.println("Sesión cerrada exitosamente.");
                            acabar = true;
                            break;
                                        
                        default:
                        System.out.println("Opción no válida.");
                            break;
                    }
        
                }

                acabar = false;
                
            }
            
        }
        

}
}