package controllers;
import java.sql.*;
import java.io.*;
import java.util.Date;

import storage.DBController;

public class Movimientos {

    private Double balance = 0.0;
    private Integer idCC = 0;

    public void listaMovimientos(int idCuenta) {
        try {
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
            String sql = String.format("SELECT movimientos.valor, movimientos.fecha, movimientos.emisor, movimientos.concepto, cuentas.idCuentaCorriente from movimientos,cuentas,cuentacorriente_has_titular,titular WHERE titular.idTitular=" + Session.idTitular + " AND titular.idTitular=cuentacorriente_has_titular.Titular_idTitular AND cuentas.idCuentaCorriente=cuentacorriente_has_titular.cuentaCorriente_idCuentaCorriente AND movimientos.cuentaCorriente_idCuentaCorriente=cuentas.idCuentaCorriente;");
            ResultSet resultats = stmt.executeQuery(sql);
            while (resultats.next()) {
                Double valor = (Double) resultats.getObject(1);
                Date fecha = (Date) resultats.getDate(2);
                String emisor = (String) resultats.getObject(3);
                String concepto = (String) resultats.getObject(4);

                System.out.println(fecha + " " + valor + " " + emisor + " " + concepto);

                balance = balance + valor;
            }
            PreparedStatement psupdate = conn.prepareStatement("UPDATE cuentas SET SaldoDisponible='" + balance + "' WHERE idCuentaCorriente=" + idCuenta + "");
            
            psupdate.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        System.out.println("Current Balance: " + balance);

    }

    public static void verSaldo() {
          
        try {
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
            String sql = String.format("SELECT cuentas.SaldoDisponible, cuentas.Nombre from cuentas,cuentacorriente_has_titular,titular WHERE titular.idTitular=" + Session.idTitular + " AND titular.idTitular=cuentacorriente_has_titular.Titular_idTitular AND cuentas.idCuentaCorriente=cuentacorriente_has_titular.cuentaCorriente_idCuentaCorriente;");
            ResultSet resultats = stmt.executeQuery(sql);
            System.out.println("Account/s balance:");

            while (resultats.next()) {
                Double saldoDisponible = (Double) resultats.getObject(1);
                String nombreCuenta = (String) resultats.getObject(2);
                System.out.println(nombreCuenta + ": " + saldoDisponible + " €");
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    public static void update(int idCuenta) {

        try {
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
            String sql = String.format("SELECT movimientos.valor, cuentas.SaldoDisponible from movimientos, cuentas WHERE cuentas.idCuentaCorriente='" + idCuenta + "';");
            ResultSet resultats = stmt.executeQuery(sql);
            Double saldo = 0.0;
            while (resultats.next()) {
                Double valor = (Double) resultats.getObject(1);
                saldo = (Double) resultats.getObject(2);
                saldo = saldo + valor;
            }
            System.out.println("Current Balance: " + saldo);
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }

    public void ingresar(int idCuenta) {
          
        try {

            Console cnsl = System.console();
    
            if (cnsl == null) {
                System.out.println(
                    "No console available");
                // return;
            };
            
            //Ask user for card number
            String valorIngreso = cnsl.readLine(
                "Cantidad a introducir: ");   
            
            Double valorIng = Double.parseDouble(valorIngreso);

            long millis=System.currentTimeMillis();  
            java.sql.Date fechaIngreso=new java.sql.Date(millis);  
        
            Connection conn = DBController.getConn();
            PreparedStatement ps = conn.prepareStatement("INSERT INTO movimientos (Valor, Fecha, Emisor, Concepto, CuentaCorriente_idCuentaCorriente, Categorias_idCategoria, Tarjeta_idTarjeta) VALUES (?,?,?,?,?,?,?)");
            ps.setDouble(1, valorIng);
            ps.setDate(2, fechaIngreso);
            ps.setString(3, "");
            ps.setString(4, "Ingreso efectivo.");
            ps.setInt(5, idCuenta);
            ps.setInt(6, 6);
            ps.setInt(7, Session.idTarjeta);

            ps.executeUpdate();


            balance = balance + valorIng;
            PreparedStatement psUpdateIng = conn.prepareStatement("UPDATE cuentas SET SaldoDisponible=" + balance + " WHERE idCuentaCorriente=" + idCuenta + "");
            psUpdateIng.executeUpdate();
            System.out.println("Current Balance: " + balance);


        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    public int obtenerIdCuenta(String iban, String numCuenta){
        
        try {
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
    
            String sel = String.format("SELECT idCuentaCorriente from cuentas WHERE IBAN='" + iban + "' AND NumeroCuenta='" + numCuenta + "';");
            ResultSet resultats = stmt.executeQuery(sel);

            while (resultats.next()) {
                idCC = (Integer) resultats.getInt(2);
                
                return idCC;
            }
            
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return idCC;

    }

    public void retirar(int idCuenta) {
        try {

            Console cnsl = System.console();
    
            if (cnsl == null) {
                System.out.println(
                    "No console available");
                // return;
            };
            
            //Ask user for card number
            String valorRetiro = cnsl.readLine(
                "Cantidad a retirar: ");   

            Double valorRet = (Double.parseDouble(valorRetiro)) * (-1);

            long millis=System.currentTimeMillis();  
            java.sql.Date fechaIngreso=new java.sql.Date(millis);  
        
            Connection conn = DBController.getConn();
            PreparedStatement ps = conn.prepareStatement("INSERT INTO movimientos (Valor, Fecha, Emisor, Concepto, CuentaCorriente_idCuentaCorriente, Categorias_idCategoria, Tarjeta_idTarjeta) VALUES (?,?,?,?,?,?,?)");
            ps.setDouble(1, valorRet);
            ps.setDate(2, (java.sql.Date) fechaIngreso);
            ps.setString(3, "");
            ps.setString(4, "Reintegro efectivo.");
            ps.setInt(5, idCuenta);
            ps.setInt(6, 6);
            ps.setInt(7, Session.idTarjeta);

            ps.executeUpdate();

            //Update the balance of the account
            balance = balance + valorRet;
            System.out.println("Current Balance: " + balance);
            PreparedStatement psUpdateIng = conn.prepareStatement("UPDATE cuentas SET SaldoDisponible=" + balance + " WHERE idCuentaCorriente=" + idCuenta + "");
            psUpdateIng.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }

    public void transferir(int idCuenta) {

        try{
            
            Console cnsl = System.console();
            if (cnsl == null) {
                System.out.println(
                    "No console available");
                // return;
            };
            
            //Ask user for quantity, and the destiny of the money
            String valorTransferencia = cnsl.readLine(
                "Cantidad a transferir: ");
            Double valorTra = (Double.parseDouble(valorTransferencia));

            String ibanDestino = cnsl.readLine(
                "Introducir IBAN de destino: ");
            String numCuentaDestino = cnsl.readLine(
                "Introducir el numero de la cuenta de destino: ");

            long millis=System.currentTimeMillis();  
            java.sql.Date fechaTransferencia = new java.sql.Date(millis);  


            //Get the account id of the given account number
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();
            String sql = String.format("SELECT cuentas.idCuentaCorriente from cuentas WHERE IBAN='" + ibanDestino + "' AND NumeroCuenta='" + numCuentaDestino + "';");
            ResultSet resultats = stmt.executeQuery(sql);

            int cuentaCorrienteD = 0;
            while (resultats.next()) {
                cuentaCorrienteD = (int) resultats.getObject(1);
            }

            //Take the money from the user's account
            Double valorRetTra = (valorTra) * (-1);
            
            PreparedStatement ps = conn.prepareStatement("INSERT INTO movimientos (Valor, Fecha, Emisor, Concepto, CuentaCorriente_idCuentaCorriente, Categorias_idCategoria, Tarjeta_idTarjeta) VALUES (?,?,?,?,?,?,?)");
            ps.setDouble(1, valorRetTra);
            ps.setDate(2, (java.sql.Date) fechaTransferencia);
            ps.setString(3, "");
            ps.setString(4, "Reintegro efectivo.");
            ps.setInt(5, idCuenta);
            ps.setInt(6, 7);
            ps.setInt(7, Session.idTarjeta);
            ps.executeUpdate();
            
            //Update the balance of the user's account
            update(idCuenta);
            System.out.println("Current Balance: " + balance);



            //To get the name of the user to show in the destiny movement
            Statement stmts = conn.createStatement();
            String sqlemi = String.format("SELECT titular.Nombre, titular.Apellido1 from titular WHERE idTitular='" + Session.idTitular +"';");
            ResultSet resultatsemi = stmts.executeQuery(sqlemi);
            String emisorNM = "";
            while (resultatsemi.next()) {
                String emisorName = (String) resultatsemi.getObject(1);
                String emisorSurname = (String) resultatsemi.getObject(2);

                emisorNM = emisorName + " " + emisorSurname;
                System.out.println(emisorNM);
            }

            //Insert the movement in the destiny account
            PreparedStatement pstr = conn.prepareStatement("INSERT INTO movimientos (Valor, Fecha, Emisor, Concepto, CuentaCorriente_idCuentaCorriente, Categorias_idCategoria, Tarjeta_idTarjeta) VALUES (?,?,?,?,?,?,?)");
            pstr.setDouble(1, valorTra);
            pstr.setDate(2, (java.sql.Date) fechaTransferencia);
            System.out.println("emisoraux: " + emisorNM);
            pstr.setString(3, emisorNM);
            pstr.setString(4, "Reintegro efectivo.");
            pstr.setInt(5, cuentaCorrienteD);
            pstr.setInt(6, 6);
            pstr.setInt(7, Session.idTarjeta);

            pstr.executeUpdate();

            //update balance of the account of destiny
            update(cuentaCorrienteD);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

}
