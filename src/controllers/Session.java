package controllers;
import java.sql.*;
import java.io.*;
import storage.DBController;

public class Session {

    public static int idTitular;
    public static int idTarjeta;

    public boolean login() {        
        boolean sesion = false;
        try {
            Console cnsl = System.console();
    
            if (cnsl == null) {
                System.out.println(
                    "No console available");
                // return;
            };
            
            //Ask user for card number
            String cardNum = cnsl.readLine(
                "Enter your card number: ");   
                
            Connection conn = DBController.getConn();
            Statement stmt = conn.createStatement();

            //Select the security number from the input card number and then save it as pin
            String sql = String.format("SELECT tarjeta.idTarjeta, Titular_idTitular, titular.pin from tarjeta, Titular WHERE NumTarjeta='" + cardNum +"' AND Titular.idTitular=Tarjeta.Titular_idTitular");
            ResultSet resultats = stmt.executeQuery(sql);
            
            int pin = 0;
            while (resultats.next()) {
                idTarjeta = (Integer) resultats.getInt(1);
                idTitular = (Integer) resultats.getInt(2);
                pin = (int) resultats.getObject(3);
                System.out.println(pin);

            }

            //Ask user for card pin
            String cardPin = cnsl.readLine("Enter your card pin: ");
            int intCardPin = Integer.parseInt(cardPin);
            System.out.println(cardPin);

            //Check if the input pin is the same as the card pin
      
            if (pin == intCardPin) {
                System.out.println("The pin you entered is correct.");
                sesion = true;
                return sesion;
                // System.out.println("Current balance: " + saldo);
            } else {
                System.out.println("The pin you have entered is incorrect.");
                return sesion;
            }
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        
      return sesion;
    }

    public void changePin() {        
        try {
            Console cnsl = System.console();
    
            if (cnsl == null) {
                System.out.println(
                    "No console available");
            };

            String newPin = cnsl.readLine("Enter your new pin: ");   
            System.out.println(newPin);

            Connection conn = DBController.getConn();
            PreparedStatement ps = conn.prepareStatement("UPDATE Titular SET pin='" + newPin + "' WHERE idTitular='" + idTitular + "'");
            ps.executeUpdate();

        } catch (SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        }
        
    }
}

